<?php

/**
 * Created by PhpStorm.
 * User: bikeshshakya
 * Date: 9/1/17
 * Time: 8:47 PM
 */
class Config
{
    static $request = ['tax' => 'TAX',
        'store' => "STORE",
        'query' => "QUERY",
        'bye' => "BYE",
        'end' => "END"
    ];
    static $response = [
        'tax' => 'TAX OK'
    ];

    public $taxRates = [
        /*[0,18200,0,0],*/
        [18201,37000,0,19],
        [37001,80000,3572,32],
        [80001,180000,17547,37],
        [180001,'~',54547,45],
    ];

}
