<?php

require 'config/config.php';

class TaxServer
{

    public $records = [];
    public $server_user = 'Server ';
    public $config;
    public $socketResource;

    public function __construct(){
        $this->config = new Config();

        $this->socketResource = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
        socket_set_option($this->socketResource, SOL_SOCKET, SO_REUSEADDR, 1);
        socket_bind($this->socketResource, 0, PORT);
        socket_listen($this->socketResource);
    }

    function send($message)
    {
        global $clientSocketArray;
        $messageLength = strlen($message);
        foreach ($clientSocketArray as $clientSocket) {
            @socket_write($clientSocket, $message, $messageLength);
        }
        return true;
    }

    function unseal($socketData)
    {
        $length = ord($socketData[1]) & 127;
        if ($length == 126) {
            $masks = substr($socketData, 4, 4);
            $data = substr($socketData, 8);
        } elseif ($length == 127) {
            $masks = substr($socketData, 10, 4);
            $data = substr($socketData, 14);
        } else {
            $masks = substr($socketData, 2, 4);
            $data = substr($socketData, 6);
        }
        $socketData = "";
        for ($i = 0; $i < strlen($data); ++$i) {
            $socketData .= $data[$i] ^ $masks[$i % 4];
        }
        return $socketData;
    }

    function seal($socketData)
    {
        $b1 = 0x80 | (0x1 & 0x0f);
        $length = strlen($socketData);

        if ($length <= 125)
            $header = pack('CC', $b1, $length);
        elseif ($length > 125 && $length < 65536)
            $header = pack('CCn', $b1, 126, $length);
        elseif ($length >= 65536)
            $header = pack('CCNN', $b1, 127, $length);
        return $header . $socketData;
    }

    function doHandshake($received_header, $client_socket_resource, $host_name, $port)
    {
        $headers = array();
        $lines = preg_split("/\r\n/", $received_header);
        foreach ($lines as $line) {
            $line = chop($line);
            if (preg_match('/\A(\S+): (.*)\z/', $line, $matches)) {
                $headers[$matches[1]] = $matches[2];
            }
        }

        $secKey = $headers['Sec-WebSocket-Key'];
        $secAccept = base64_encode(pack('H*', sha1($secKey . '258EAFA5-E914-47DA-95CA-C5AB0DC85B11')));
        $buffer = "HTTP/1.1 101 Web Socket Protocol Handshake\r\n" .
            "Upgrade: websocket\r\n" .
            "Connection: Upgrade\r\n" .
            "WebSocket-Origin: $host_name\r\n" .
            "WebSocket-Location: ws://$host_name:$port/simplechat/shout.php\r\n" .
            "Sec-WebSocket-Accept:$secAccept\r\n\r\n";
        socket_write($client_socket_resource, $buffer, strlen($buffer));
    }

    function newConnectionACK($client_ip_address)
    {
        $message = 'New client ' . $client_ip_address . ' joined';
        $messageArray = array('message' => $message, 'message_type' => 'chat-connection-ack');
        $ACK = $this->seal(json_encode($messageArray));
        return $ACK;
    }

    function connectionDisconnectACK($client_ip_address)
    {
        $message = 'Client ' . $client_ip_address . ' disconnected';
        $messageArray = array('message' => $message, 'message_type' => 'chat-connection-ack');
        $ACK = $this->seal(json_encode($messageArray));
        return $ACK;
    }

    function createChatBoxMessage($chat_user, $chat_box_message)
    {
        $message = $chat_user . ": <div class='chat-box-message'>" . nl2br($chat_box_message) . "</div>";
        $messageArray = array('message' => $message, 'message_type' => 'chat-box-html');
        $chatMessage = $this->seal(json_encode($messageArray));
        return $chatMessage;
    }

    function tax($data = Null)
    {
        $this->printMessage('TAX : OK');
    }

    function store($data){
        $data = explode(PHP_EOL,$data);
        unset($data[0]);
        if(count($data) == 4){
            $this->config->taxRates[] = $data;
            $this->query();
            $this->printMessage('STORE : OK');
        }else{
            $this->printMessage('STORE : INVALID DATA');
        }

    }

    function query($data = Null){
        $str = '';
        foreach($this->config->taxRates as $r){
            $str .= implode(' ',$r)."<br/>";
        }
        if(!is_null($data)){
            $this->printMessage($str);
            $this->printMessage('OK','Query ');
        }else{
            return true;
        }
    }

    function bye($data = Null){
        $this->printMessage('BYE : OK');
    }

    function end($data = Null){
        $this->printMessage('END : OK');
        exit;
    }

    function calculateTax($data){

        $myValue = $data;
        $taxresult = Null;
        foreach ($this->config->taxRates as $r){
            if($r[1] != '~'){
                if ($myValue >= $r[0] && $myValue <= $r[1]) {
                    $taxresult = $r[2] + ($data * ($r[3]/100));
                    $this->printMessage('TAX IS '.$taxresult);
                    break;
                }
            }else{
                if($myValue >= $r[0]){
                    $taxresult = $r[2] + ($data * ($r[3]/100));
                    $this->printMessage('TAX IS '.$taxresult);
                    break;
                }
            }
        }

        if(is_null($taxresult))
            $this->printMessage('I DON’T KNOW '.$data);
    }

    function printMessage($command,$user=Null){
        $user = ($user)?$user : $this->server_user;
        $chat_box_message = $this->createChatBoxMessage($user, $command);
        $this->send(nl2br($chat_box_message));
    }
}

?>
