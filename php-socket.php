<?php
define('HOST_NAME', "127.0.0.1");
define('PORT', "8090");
$null = NULL;

require_once("taxserver.php");
require_once('config/config.php');
$config = new Config();
$clientHandler = new TaxServer();

$clientSocketArray = array($clientHandler->socketResource);
while (true) {

    $newSocketArray = $clientSocketArray;
    socket_select($newSocketArray, $null, $null, 0, 10);

    if (in_array($clientHandler->socketResource, $newSocketArray)) {
        $newSocket = socket_accept($clientHandler->socketResource);
        $clientSocketArray[] = $newSocket;
        $header = socket_read($newSocket, 1024);
        $clientHandler->doHandshake($header, $newSocket, HOST_NAME, PORT);

        socket_getpeername($newSocket, $client_ip_address);
        $connectionACK = $clientHandler->newConnectionACK($client_ip_address);

        $clientHandler->send($connectionACK);

        $newSocketIndex = array_search($clientHandler->socketResource, $newSocketArray);
        unset($newSocketArray[$newSocketIndex]);
    }

    foreach ($newSocketArray as $newSocketArrayResource) {
        while (socket_recv($newSocketArrayResource, $socketData, 1024, 0) >= 1) {
            $socketMessage = $clientHandler->unseal($socketData);
            $messageObj = json_decode($socketMessage);
            if($messageObj){
                $chat_box_message = $clientHandler->createChatBoxMessage($messageObj->chat_user, $messageObj->chat_message);
                $clientHandler->send(nl2br($chat_box_message));

                if(is_numeric($messageObj->chat_message)){
                    $clientHandler->calculateTax($messageObj->chat_message);
                }else {
                    if (in_array(substr(strtolower($messageObj->chat_message),0,5), array_keys(Config::$request))) {
                        foreach (Config::$request as $r) {
                            if (substr(strtolower($messageObj->chat_message),0,5) == strtolower($r)) {
                                $clientHandler->{$r}($messageObj->chat_message);
                            }
                        }
                    }
                }
            }
            break 2;
        }

        $socketData = @socket_read($newSocketArrayResource, 1024, PHP_NORMAL_READ);
        if ($socketData === false) {
            socket_getpeername($newSocketArrayResource, $client_ip_address);
            $connectionACK = $clientHandler->connectionDisconnectACK($client_ip_address);
            $clientHandler->send($connectionACK);
            $newSocketIndex = array_search($newSocketArrayResource, $clientSocketArray);
            unset($clientSocketArray[$newSocketIndex]);
        }
    }
}
socket_close($clientHandler->socketResource);
